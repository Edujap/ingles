<?php



$router->get('/', function () use ($router) {
    return 'INGLESITO'; //localhost:8000
});

$router->group(['prefix' =>'api'], function () use($router){
    $router->get('word', 'wordController@index');//localhost:8000/api/word
    $router->get('word/{id}', 'wordController@show');
    $router->post('word', 'wordController@store');
    $router->put('word/{id}', 'wordController@update');
    $router->delete('word/{id}', 'wordController@destroy');

});
$router->group(['prefix' =>'api'], function () use($router){
    $router->get('meaning', 'meaningController@index');//localhost:8000/api/meaning
    $router->get('meaning/{id}', 'meaningController@show');
    $router->post('meaning', 'meaningController@store');
    $router->put('meaning/{id}', 'meaningController@update');
    $router->delete('meaning/{id}', 'meaningController@destroy');

});
$router->group(['prefix' =>'api'], function () use($router){
    $router->get('sinonimo', 'sinonimoController@index');
    $router->get('sinonimo/{id}', 'sinonimoController@show');
    $router->post('sinonimo', 'sinonimoController@store');
    $router->put('sinonimo/{id}', 'sinonimoController@update');
    $router->delete('sinonimo/{id}', 'sinonimoController@destroy');

});


$router->group(['prefix' =>'api'], function () use($router){

    $router->get('meaning_has', 'meaning_hasController@index');
    $router->get('meaning_has/{id}', 'meaning_hasController@show');
    $router->post('meaning_has', 'meaning_hasController@store');//localhost:8000/api/meaning_has
    $router->put('meaning_has/{id}', 'meaning_hasController@update');
    $router->delete('meaning_has/{id}', 'meaning_hasController@destroy');
    //////// $router->delete('has_meaning/{id}', 'meaning_has@destroy');

});
$router->group(['prefix' =>'api'], function () use($router){
    $router->get('sinonimo_has', 'sinonimo_HasController@index');
    $router->get('sinonimo_has/{id}', 'sinonimo_HasController@show');
    $router->post('sinonimo_has', 'sinonimo_HasController@store');
    $router->put('sinonimo_has/{id}', 'sinonimo_HasController@update');
    $router->delete('sinonimo_has/{id}', 'sinonimo_HasController@destroy');

});
