<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWordHasSynonymousTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('word_has_synonymous', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('word_id')->unsigned();
                $table->integer('sinonimo_id')->unsigned();
                $table->foreign('word_id')->references('id')->on('word');
                $table->foreign('sinonimo_id')->references('id')->on('sinonimo');
                $table->timestamps();
        //
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('word_has_synonymous', function (Blueprint $table) {
            //
        });
    }
}
