<?php
    $allowedResourceTypes=[
        'word',
        'meaning',
        'sinonimo',
        'word_has_significance',
        'word_has_synonymous'
    ];
    
    $resourceType=$_GET['resource_type'];
    if(!in_array($resourceType,$allowedResourceTypes)){
        die;
    }

    $meaning=[
        1=>['significado'=> 'Tiene bananas',],
        2=>['significado'=> 'Tiene bananas',],
        3=>['significado'=> 'Tiene bananas',],
        4=>['significado'=> 'Tiene bananas',]
    ];
    $sinonimo=[
        1=>['palabra'=> 'tiene platanos',],
        2=>['palabra'=> 'tiene cambur',],
    ];
    $word=[
        1=>['word_text'=> 'Tiene bananas','tech_term'=> 'yes ','firs_letter'=>'T'],
        2=>['word_text'=> 'Tiene bananas','tech_term'=> 'yes ','firs_letter'=>'T'],
        3=>['word_text'=> 'Tiene bananas','tech_term'=> 'yes ','firs_letter'=>'T'],
        4=>['word_text'=> 'Tiene bananas','tech_term'=> 'yes ','firs_letter'=>'T'],
    ];
    $word_has_synonymous=[
        1=>['word_id'=> 2 , 'meaning_id'=> 2],
        2=>['word_id'=> 2 , 'meaning_id'=> 2],
    ];
    $word_has_significance=[
        1=>['word_id'=> 1 , 'meaning_id'=> 1],
        2=>['word_id'=> 2 , 'meaning_id'=> 2],
        3=>['word_id'=> 3 , 'meaning_id'=> 3],
        4=>['word_id'=> 4 , 'meaning_id'=> 4],
    ];
    
    
    header('Content-Type:application/json');
 
    $resourceid=array_key_exists('resource_id',$_GET)? $_GET['resource_id']:'';
    switch(strtoupper($_SERVER['REQUEST_METHOD'])){
        case 'GET':
            if(empty($resourceid)){
                if($resourceType=='word'){
                    echo json_encode($word);
                }
                else if($resourceType=='meaning'){
                    echo json_encode($meaning);
                }
                else if($resourceType=='sinonimo'){
                    echo json_encode($sinonimo);
                }
                else if($resourceType=='word_has_significance'){
                    echo json_encode($word_has_significance);
                }
                else if($resourceType=='word_has_synonymous'){
                    echo json_encode($word_has_synonymous);
                }
                else{
                    die;
                }}

            else if($resourceType=='word'){
                    echo json_encode($word[$resourceid]);
                }
            else if($resourceType=='meaning'){
                    echo json_encode($meaning[$resourceid]);
                }
            else if($resourceType=='sinonimo'){
                    echo json_encode($sinonimo[$resourceid]);
                }
            else if($resourceType=='word_has_significance'){
                    echo json_encode($word_has_significance[$resourceid]);
                }
            else if($resourceType=='word_has_synonymous'){
                    echo json_encode($word_has_synonymous[$resourceid]);
                }
            else{
                    die;
                }

        break;
        case 'POST':
                $json=file_get_contents('php://input');
                $word[]=json_decode($json,true);
                //echo array_keys($books)[count($books)-1];
                echo json_encode($word);
            break;
        case 'PUT':
            echo "tu puto";
        
        break;
    }
    ?>